#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>
#include <stdarg.h>

#define ERROR_PREFIX "[1;4;48;2;255;0;0mERROR: "

#define ESC_BOLD "[1m"
#define ESC_UNDERLINE "[4m"
#define ESC_RESET "[0m"

#define ERRPUTS(str) fputs(ERROR_PREFIX str ESC_RESET, stderr)
#define ERRPRINTF(fmt, ...) fprintf(stderr, fmt, __VA_ARGS__)

void printusage (char* prog_name);

int
main (int argc, char** argv)
{
	if (argc < 2) {
		ERRPRINTF("No arguments supplied. (Recieved argc: %d)\n", argc);
		printusage(*argv);
		return -1;
	}


	int i = 1;
	int file_count = argc - 1;
	unsigned long long counter = 1, total_skip = 0;

	// parse options
	for (; i < argc; ++i)
	{
		char* arg = argv[i];
		char* end_ptr;

		if (arg[0] == '-') // option
		{
			--file_count;

			switch (arg[1]) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				total_skip += strtoull(arg + 1, &end_ptr, 10);
				if (*end_ptr || arg + 1 == end_ptr)
					goto Bad_number;
				break;
			case '-': // long option
				if (!arg[2])
					++total_skip;
				else if (isdigit(arg[2])) {
					char* end_ptr;
					total_skip += strtoull(arg + 2, &end_ptr, 10);
					if (*end_ptr)
						goto Bad_number;
				}
				else if (!memcmp(arg + 2, "help", 4)) {
					printusage(*argv);
					return 0;
				}
				else if (!memcmp(arg + 2, "starting-value", 14)) {
					char *end_ptr, *start_ptr;
					if (arg[16] == '=')
						start_ptr = arg + 17;
					else if (!arg[16]) {
						++i;
						start_ptr = argv[i];
					} else
						goto Unknown_arg;

					counter = strtoull(start_ptr, &end_ptr, 10);
					
					if (*end_ptr || start_ptr == end_ptr)
						goto Bad_number;
				}
				else
					goto Unknown_arg;

				break;
			case 'h':
				printusage(*argv);
				return 0;
			case 's':
				++i;
				--file_count;
				counter = strtoull(argv[i], &end_ptr, 10);
				if (*end_ptr || argv[i] == end_ptr)
					goto Bad_number;
				continue; // i got inc'ed
			default:
				goto Unknown_arg;
			}
		}
	}

	if (file_count <= 0) {
		ERRPUTS("No filenames supplied.\n");
		return -1;
	}

	int padd_count = log10(file_count + total_skip + counter - 1) + 1;

	// rename files
	for (int i = 1; i < argc; ++i)
	{
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				counter += strtoull(argv[i] + 2, NULL, 10);
				break;
			case '-':
				if (isdigit(argv[i][2]))
					counter += strtoull(argv[i] + 2, NULL, 10);
				else if (!argv[i][2])
					++counter;
				else if (!memcmp(argv[i] + 2, "starting-value=", 15)) {
					++i;
					continue;
				}
				break;
			case 's':
				++i;
				continue;
			}

			continue;
		}
		
		size_t len = strlen(argv[i]);
		char* new = malloc(len + padd_count + 1); // 1 for '-'

		if (new == NULL) {
			ERRPRINTF("Malloc failed for file counter %llu.\nstrerror(%d): %s\n", counter, errno, strerror(errno));
		}

		sprintf(new, "%.*llu-%s", padd_count, counter++, argv[i]);

		if (rename(argv[i], new)) {
			ERRPRINTF("Rename failed for filename '%s' (attempted change: '%s').\nstrerror(%d): %s\n", argv[i], new, errno, strerror(errno));
			return -1;
		}

	}

	printf("Renamed %d files, padding prefix to %d digit(s)\n", file_count, padd_count);

	return 0;

Bad_number:
	ERRPRINTF("Skip/start value must be positive base-10 integer (Found: %s).\n", argv[i] + 1);
	return -1;

Unknown_arg:
	ERRPRINTF("Unknown option '%s' found at arg %d.\n", argv[i], i);
	return -1;
}

void
printusage (char* prog_name)
{
	printf(
		"%s [options] [filename... || modifiers...]\n\n"
		ESC_BOLD"Rename a list of files to that same list with numerical order of the arguments prefixed to the filename. Modifiers can be used to alter the value of the counter. E.G: '%s somestuff.txt -- morestuff.txt' will result in the two files being renamed to '1-somestuff.txt' and '3-morestuff.txt', with the '--' skipping the '2'\n\n"ESC_RESET
		ESC_BOLD"OPTIONS:\n"ESC_RESET
			"-h, --help             :  Show this usage message\n"
			"-<number>, --<number>  :  Increment the counter by <number> (unsigned long long, base-10) when this argument's position in the option order is reached. '--' without a <number? will be interpreted as 1, but '-' will NOT\n"
			"-s <number>, --starting-value=<number>, --starting-value <number>  :  Counter's starting value (unsigned long long, base-10)\n"
		ESC_UNDERLINE ESC_BOLD"MODIFIERS:\n"ESC_RESET
			"--                     :  Increment the counter by one. Behaves like creating a temporary file to \"absorb\" a givin count.\n"
			"-<number>, --<number>  :  Add number (unsigned long long, base-10) to the counter"
		, prog_name, prog_name);
}
