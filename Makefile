OUT_DIR := .
INSTALL_DIR := ~/bin

all: numf

numf: main.c
	gcc -O3 -lm main.c -o $(OUT_DIR)/numf
	strip -s $(OUT_DIR)/numf

clean:
	rm -f $(OUT_DIR)/numf

uninstall:
	rm -f $(INSTALL_DIR)/numf

install: numf
	install $(OUT_DIR)/numf $(INSTALL_DIR)/numf

.PHONY: install clean uninstall
