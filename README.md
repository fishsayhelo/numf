# numf
Program to prefix file names with numbers based on argument order

## building:
To compile & link:
`make`
To clean:
`make clean`

## installing:
`make install` as root or copy resulting binary into path location

## uninstalling:
`make uninstall` or remove binary from path (`rm (which numf)`)

### fishsayhelo
